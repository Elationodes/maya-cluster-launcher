# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/local" {
  version = "2.1.0"
  hashes = [
    "h1:/OpJKWupvFd8WJX1mTt8vi01pP7dkA6e//4l4C3TExE=",
    "h1:EYZdckuGU3n6APs97nS2LxZm3dDtGqyM4qaIvsmac8o=",
    "h1:KfieWtVyGWwplSoLIB5usKAUnrIkDQBkWaR5TI+4WYg=",
    "h1:KtUCltnScfZbcvpE9wPH+a0e7KgMX4w7y8RSxu5J/NQ=",
    "zh:0f1ec65101fa35050978d483d6e8916664b7556800348456ff3d09454ac1eae2",
    "zh:36e42ac19f5d68467aacf07e6adcf83c7486f2e5b5f4339e9671f68525fc87ab",
    "zh:6db9db2a1819e77b1642ec3b5e95042b202aee8151a0256d289f2e141bf3ceb3",
    "zh:719dfd97bb9ddce99f7d741260b8ece2682b363735c764cac83303f02386075a",
    "zh:7598bb86e0378fd97eaa04638c1a4c75f960f62f69d3662e6d80ffa5a89847fe",
    "zh:ad0a188b52517fec9eca393f1e2c9daea362b33ae2eb38a857b6b09949a727c1",
    "zh:c46846c8df66a13fee6eff7dc5d528a7f868ae0dcf92d79deaac73cc297ed20c",
    "zh:dc1a20a2eec12095d04bf6da5321f535351a594a636912361db20eb2a707ccc4",
    "zh:e57ab4771a9d999401f6badd8b018558357d3cbdf3d33cc0c4f83e818ca8e94b",
    "zh:ebdcde208072b4b0f8d305ebf2bfdc62c926e0717599dcf8ec2fd8c5845031c3",
    "zh:ef34c52b68933bedd0868a13ccfd59ff1c820f299760b3c02e008dc95e2ece91",
  ]
}

provider "registry.terraform.io/hashicorp/template" {
  version = "2.2.0"
  hashes = [
    "h1:0wlehNaxBX7GJQnPfQwTNvvAf38Jm0Nv7ssKGMaG6Og=",
    "h1:12Bac8B6Aq2+18xe8iqp5iYytav2Bw+jG43z/VaK5zI=",
    "h1:94qn780bi1qjrbC3uQtjJh3Wkfwd5+tTtJHOb7KTg9w=",
    "h1:LN84cu+BZpVRvYlCzrbPfCRDaIelSyEx/W9Iwwgbnn4=",
    "zh:01702196f0a0492ec07917db7aaa595843d8f171dc195f4c988d2ffca2a06386",
    "zh:09aae3da826ba3d7df69efeb25d146a1de0d03e951d35019a0f80e4f58c89b53",
    "zh:09ba83c0625b6fe0a954da6fbd0c355ac0b7f07f86c91a2a97849140fea49603",
    "zh:0e3a6c8e16f17f19010accd0844187d524580d9fdb0731f675ffcf4afba03d16",
    "zh:45f2c594b6f2f34ea663704cc72048b212fe7d16fb4cfd959365fa997228a776",
    "zh:77ea3e5a0446784d77114b5e851c970a3dde1e08fa6de38210b8385d7605d451",
    "zh:8a154388f3708e3df5a69122a23bdfaf760a523788a5081976b3d5616f7d30ae",
    "zh:992843002f2db5a11e626b3fc23dc0c87ad3729b3b3cff08e32ffb3df97edbde",
    "zh:ad906f4cebd3ec5e43d5cd6dc8f4c5c9cc3b33d2243c89c5fc18f97f7277b51d",
    "zh:c979425ddb256511137ecd093e23283234da0154b7fa8b21c2687182d9aea8b2",
  ]
}

provider "registry.terraform.io/hetznercloud/hcloud" {
  version     = "1.27.2"
  constraints = ">= 1.27.0"
  hashes = [
    "h1:8aye/uFw+BUY5fFuWivKMZvH63Xk/d+K5b31PYkgcTA=",
    "h1:dVF6enmonTn3FoL4pXmk6W/bdQEEWEemuGCih2nbSV8=",
    "h1:vIrSTDNmnHO/YHfqGWk20RU4C6Q9QpDYmffnEz+1jnk=",
    "h1:viWHrucVM0McdtooNMSozQz92sWtGJN+FpgrWYYAM5M=",
    "zh:0294686d56f01d562a9ab654e7a6b2c29fdbad37e4fcdb8f8a220bfccf193de7",
    "zh:0a159d762616e94fac9dbe7d90c1b6892aa3ca494f48100e7b5c7dbd891cd5ee",
    "zh:106bdbec2288dc7e0ba8c69bb8d1d8b73deb70bc6247027132079086ea4dc613",
    "zh:26b6217c52158172a5cfb377c8a51c1a53589a53e1f7118e9ce407ab772fd751",
    "zh:404610c460ad942b64df4162ae97ac8f04679e4e1385905e253e357cdc2179fa",
    "zh:729ae99602b8cfbde655d9456886c42eb7d8c99d09edc9987cb80176b3056a05",
    "zh:7f9851952f1e43707e6b9393551a609eee32d58238d4cea876301441354f4077",
    "zh:8e0debc2b24ab6d8f8517a4a91759894555458df92b7cd213e582b6d32a6b8a9",
    "zh:8ff3bc4e8cd2154d76d14af6b97f029f6ccdbdc9cd37b254bade61e8ae59d87d",
    "zh:9fe93ff81ecca5aec5260c14688d4ee81aab89cb471f10057398562b0294de26",
    "zh:b844d04e422c12f2f421e7904db8d57b2be80c1a77f8cfca1e793e5d4a67b7c3",
    "zh:c45e8d7def6a52353e52b6b0baafcda0ad012344563d34706216f490ecabb0a7",
    "zh:e121ffee1f491dc1be901075b94e01e45611eec368b43a35c25b00a17d319315",
  ]
}
